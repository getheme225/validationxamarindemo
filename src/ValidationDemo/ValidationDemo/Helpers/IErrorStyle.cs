using Xamarin.Forms;

namespace ValidationDemo.Helpers
{
    public interface IErrorStyle
    {
        void ShowError(View view, string message);
        void RemoveError(View view);
    }
}