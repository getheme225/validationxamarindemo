using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using ValidationDemo.Validators.Contracts;
using Xamarin.Forms;
using System.Linq;
using System.Text;
using ValidationDemo.Helpers;

namespace ValidationDemo.Behaviors
{
    public class ValidationBehavior : Behavior<View>
    {
        IErrorStyle _style = new BasicErrorStyle();
        
        /// <summary>
        /// The property which value is validated
        /// </summary>
        public string PropertyName { get; set; }
       
        /// <summary>
        ///  A global validation behavior which is attached to the parent layout
        /// </summary>
        public ValidationGroupBehavior Group { get; set; } 

        private View _view;
        
        /// <summary>
        /// the validation rules
        /// </summary>
        public ObservableCollection<IValidator> Validators { get; set; } = new ObservableCollection<IValidator>();
        
        public bool Validate()
        {
            var isValid = true;

            foreach (var validator in Validators)
            {
                var result = validator.Check(_view.GetType()
                                                    .GetProperty(PropertyName)?
                                                    .GetValue(_view));
                isValid = isValid && result;
                if (!isValid)
                {
                    _style.ShowError(_view, validator.ErrorMessage);
                    break;
                }
                _style.RemoveError(_view);
            }
            
            return isValid;
        }

        protected override void OnAttachedTo(BindableObject bindable)
        {
            base.OnAttachedTo(bindable);

            _view = bindable as View;
            _view.PropertyChanged += OnPropertyChanged;
            _view.Unfocused += OnUnFocused;

            if (Group != null)
            {
                Group.Add(this);
            }
        }

        protected override void OnDetachingFrom(BindableObject bindable)
        {
            base.OnDetachingFrom(bindable);
            
            _view.PropertyChanged -= OnPropertyChanged;
            _view.Unfocused -= OnUnFocused;

            if (Group != null)
            {
                Group.Remove(this);
            }
        }

        private void OnUnFocused(object sender, FocusEventArgs e)
        {
            Validate();
            if (Group != null)
            {
                Group.Update();
            }
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PropertyName)
            {
                Validate();
            }
        }
        
        private string ErrorMessageFormater(List<string> errors)
        {
            var stringBuilder = new StringBuilder();
            errors.Select(x => stringBuilder.Append($"{x}\n"));
            return stringBuilder.ToString();
        }
    }
    
}