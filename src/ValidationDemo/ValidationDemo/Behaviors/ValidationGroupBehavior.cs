using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace ValidationDemo.Behaviors
{
    public class ValidationGroupBehavior : Behavior<VisualElement>
    {
        #region BindableProperties
        public static readonly BindableProperty IsValidProperty =
            BindableProperty.Create("IsValid",
                typeof(bool),
                typeof(ValidationGroupBehavior),
                false,
                BindingMode.TwoWay);

        public bool IsValid
        {
            get => (bool) GetValue(IsValidProperty);
            set => SetValue(IsValidProperty, value);
        }
        #endregion
        
        private List<ValidationBehavior> _validationBehaviors;

        public ValidationGroupBehavior()
        {
            _validationBehaviors = new List<ValidationBehavior>();
        }

        protected override void OnAttachedTo(BindableObject bindable)
        {
            base.OnAttachedTo(bindable);
            BindingContext = bindable.BindingContext;
        }

        protected override void OnDetachingFrom(BindableObject bindable)
        {
            BindingContextChanged -= OnBindingContextChanged;
        }

        private void OnBindingContextChanged(object sender, EventArgs e)
        {
            BindingContext = ((BindableObject) sender).BindingContext;
        }

        public void Add(ValidationBehavior validationBehavior)
        {
            _validationBehaviors.Add(validationBehavior);
        }

        public void Remove(ValidationBehavior validationBehavior)
        {
            _validationBehaviors.Remove(validationBehavior);
        }

        public void Update()
        {
            var isValid = true;
            foreach (var validationBehavior in _validationBehaviors)
            {
                isValid = isValid && validationBehavior.Validate();
            }

            IsValid = isValid;
        }
    }
}