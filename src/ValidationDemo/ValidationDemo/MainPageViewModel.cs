using System.Threading.Tasks;
using System.Windows.Input;
using Acr.UserDialogs;
using ValidationDemo.Helpers;
using Xamarin.Forms;

namespace ValidationDemo
{
    public class MainPageViewModel : BindableBase
    {
        public bool CanSubmit
        {
            get => Get(false);
            set => Set(value);
        }
        
        public ICommand SubmitCommand => new Command(SubmitExecute);

        private void SubmitExecute()
        {
            UserDialogs.Instance.Alert("Submited");
        }
    }
}