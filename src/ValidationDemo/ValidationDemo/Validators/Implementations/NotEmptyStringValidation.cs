using System;
using ValidationDemo.Validators.Contracts;

namespace ValidationDemo.Validators.Implementations
{
    public class NotEmptyStringValidation : IValidator
    {
        public string ErrorMessage { get; set; } 
       
        /// <summary>
        /// You validation logic
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public bool Check(object value)
        {
            if (value is string str)
            {
                return !string.IsNullOrWhiteSpace(str);
            }

            return false;
        }
    }
}