using ValidationDemo.Validators.Contracts;

namespace ValidationDemo.Validators.Implementations
{
    public class NotNullValidation : IValidator
    {
        public string ErrorMessage { get; set; }
        
        public bool Check(object value)
        {
            return value != null;
        }
    }
}