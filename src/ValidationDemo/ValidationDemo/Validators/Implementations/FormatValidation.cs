using System.Text.RegularExpressions;
using ValidationDemo.Validators.Contracts;

namespace ValidationDemo.Validators.Implementations
{
    public class FormatValidation : IValidator
    {
        public string Format { get; set; }
        
        public string ErrorMessage { get; set; }
        
        public  bool Check(object value)
        {
            var strValue = (string) value;
            if (string.IsNullOrEmpty(strValue))
            {
                return false;
            }
            
            var regFormat = new Regex(Format);
            return regFormat.IsMatch(strValue);
        }
    }
}