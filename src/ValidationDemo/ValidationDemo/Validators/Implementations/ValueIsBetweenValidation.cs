using ValidationDemo.Validators.Contracts;

namespace ValidationDemo.Validators.Implementations
{
    public class MaxValueValidation: IValidator
    {
        public string ErrorMessage { get; set; }
        
        public int MaximumValue { get; set; }
        
        public int MinimumValue { get; set; }
        
        public bool Check(object value)
        {
            var intValue = (int) value;
            return intValue < MaximumValue && intValue > MinimumValue;
        }
    }
}