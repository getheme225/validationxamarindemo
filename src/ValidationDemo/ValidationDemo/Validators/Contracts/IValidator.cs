namespace ValidationDemo.Validators.Contracts
{
    public interface IValidator
    {
        string ErrorMessage { get; set; }
        bool Check(object value);
    }
}