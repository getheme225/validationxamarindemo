# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick demo of Xamarin form Validation

### How do I get set up? ###

* The mains file are :
 * IValidator -> an interfaces for you validation Rules
 
 * ValidationBehavior -> a behavior for validation on a concrete view, 
						-PropretyName : is the property which value you need to validate
                        
 * ValidationGroupBehavior -> a Behavior which should be attached to the Page, To validate all controls which has ValidationBehavior,
						  - IsValid: A bindable property for the validation Result
